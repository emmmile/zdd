FROM gcc:4.9

RUN apt-get update && \
    apt-get -y install cmake libboost-all-dev

COPY . /src
WORKDIR /src
RUN cmake .
RUN make
CMD ["./test-zdd"]