#!/usr/bin/python

import os, sys
from subprocess import Popen, PIPE

lengths  = [16, 32, 64, 128] # chars
sizes    = [1] # megabytes
exe      = ["substrings","random -a \"AB\"", "random -a \"ABCD\"", "random -a \"ABCDEFGH\"", "random -a \"ABCDEFGHILMNOPQR\""]
modes     = [0, 1] # 0 are arrays, 1 hash tables
tempfile = "build/test.txt"

modestrings = ["array", "hash"]

for e in exe:

    for m in modes:
        mode = modestrings[m]

        sys.stdout.write( e + " [" + ', '.join(map(str, lengths)) + "] " + mode + ": & " )
        sys.stdout.flush()
        for l in lengths:
            # compiles
            os.chdir("build")
            cmake = "cmake -DMAX_LENGTH=" + str(l) + " .."
            process = Popen( cmake.split(), stdout=PIPE )
            stdout, stderr = process.communicate()
            make = "make -j 4"
            process = Popen( make.split(), stdout=PIPE )
            stdout, stderr = process.communicate()
            os.chdir("..")

            for s in sizes:
                # build the strings
                cmd = "./build/" + str(e) + " -l " + str(l) + " -t " + str( s * 1024 * 1024 )
                process = Popen(cmd.split(), stdout=PIPE)
                stdout, stderr = process.communicate()
                if stderr != None:
                    print( stderr.decode("ascii").strip() )


                temp = open( tempfile, "wb" )
                temp.write( stdout )
                temp.close()

                benchmark = "./build/test-sdd -m " + str(m) + " -b -i " + tempfile
                process = Popen(benchmark.split(), stdout=PIPE)
                stdout, stderr = process.communicate()


                #print( e + " l=" + str(l) + " s=" + str(s) + " " + mode + ": " + stdout.decode("ascii").strip() )
                #print( stdout.decode("ascii").strip() )
                if stderr != None:
                    print( stderr.decode("ascii").strip() )
                output = float( stdout.decode("ascii").strip() )
                #output = output / 1024.0
                sys.stdout.write( "%.3f & " % output )
                sys.stdout.flush()
        sys.stdout.write( "\\\\\n" )
